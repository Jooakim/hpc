#include <assert.h>
#include <stdio.h>

#include "mpi.h"

int main(int argc, char **argv) {
    int mpi_rank, world_size, worker_count = 0;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size); 
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);


    if (mpi_rank == 0) {
        MPI_Send(&worker_count, 1, MPI_INT, 1, 
                0, MPI_COMM_WORLD);

        MPI_Recv(&worker_count, 1, MPI_INT, world_size-1,
                0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        printf ("Total number of workers: %d\n", worker_count);

    } else {
        MPI_Recv(&worker_count, 1, MPI_INT, mpi_rank-1,
                0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        worker_count++;
        if (mpi_rank < world_size-1) {
            MPI_Send(&worker_count, 1, MPI_INT, mpi_rank+1,
                    0, MPI_COMM_WORLD);
        } else {
            MPI_Send(&worker_count, 1, MPI_INT, 0,
                    0, MPI_COMM_WORLD);
        }
    }

    MPI_Finalize();
    return 0;
}
