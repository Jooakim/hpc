#include <climits>
#include <mpi.h>

#include "vertex.hh"
#include "dijkstras.hh"

using namespace std; 

void get_shortest_path(int source, int * subpool, vector<map<int,int> > vertices, int size, int sub_size) {
    int local_dist[size];
    int global_dist[size];
    int visited[size];
    int n_visited = 0;
    int mpi_rank; 

    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);

    for (size_t i = 0; i < size; i++) {
        visited[i] = 0;
        local_dist[i] = INT_MAX;
    }
    local_dist[source] = 0;
    while (n_visited < size) {

        /* Gather all distances at the root worker */
        MPI_Reduce(&local_dist, &global_dist, size, MPI_INT,
                MPI_MIN, 0, MPI_COMM_WORLD);

        int u, dist;
        /* Let the root worker find the vertex with minimum distance */
        if (mpi_rank == 0) {
            int min = INT_MAX;
            for (size_t i = 0; i < size; ++i) {
                if (global_dist[i] < min && !visited[i]) {
                    min = global_dist[i];
                    u = i;
                } 
            }
            dist = min;
        }
        /* 
         * Broadcast the next vertex in line and it's real distance from
         * the source vertex 
         */
        MPI_Bcast(&u, 1, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast(&dist, 1, MPI_INT, 0, MPI_COMM_WORLD);

        visited[u] = 1;
        n_visited++;

        /*
         * Let all worker update distances from the vertex to neighbors in
         * each worker's subpool
         */
        for (size_t ix = 0; ix < sub_size; ++ix) {
            auto search = vertices[subpool[ix]].find(u);
            if (search != vertices[subpool[ix]].end() && !visited[subpool[ix]]) {
                int temp_dist = dist + search->second;
                if (temp_dist < local_dist[subpool[ix]]) {
                    local_dist[subpool[ix]] = temp_dist;
                }
            }
        }
    }
}
