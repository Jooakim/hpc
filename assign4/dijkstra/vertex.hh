#include <vector>
struct edge {
    int weight;
    int v1;
    int v2;
};

struct vertex {
    int n2;
    int weight;
    vertex(int arg_n2, int arg_weight)
        : n2(arg_n2), weight(arg_weight) { }
};

#ifndef GRAPH_H
#define GRAPH_H

class Graph {
    int n_vertices, n_edges;
    public:
        Graph(int, int, std::string);
        void add_vertex(int, int, int);
        int get_size(void);
        struct vertex get_vertex(int);
    private:
        std::vector<struct vertex> vertices;
};

#endif
