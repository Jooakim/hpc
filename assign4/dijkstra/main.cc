#include <mpi.h>
#include <set>
#include <fstream>

#include "vertex.hh"
#include "dijkstras.hh"

using namespace std;

#define BILLION 1e9L

void benchmark_graph(string, int);

int main(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);
    
    benchmark_graph("graph_example_5_100", 100);
    benchmark_graph("graph_example_100_2000", 2000);
    benchmark_graph("graph_example_100_10000", 10000);
    benchmark_graph("graph_example_1000_10000", 10000);

    MPI_Finalize();
    return 0;
}

/*
 * Function for reading a graph from file and loading it
 * into a vertex-structure consisting of a vector of a map
 * which contains each nodes neighbours
 */
void benchmark_graph(string file, int size) {
    int mpi_rank, world_size; 
    int * pool_vertices;
    int * subpool;
    int id, id2, weight;
    vector<map<int,int> > vertices(size);

    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    int sub_size = size/world_size;

    /* Read graph from file */
    ifstream infile(file.c_str());
    while (infile >> id >> id2 >> weight) {
        vertices[id].insert(make_pair(id2, weight));
        vertices[id2].insert(make_pair(id, weight));
    }


    /* Create array which contains all vertex id's*/
    if (mpi_rank == 0) {
        pool_vertices = (int*) malloc(size*sizeof(int));
        for (size_t i = 0; i < size; ++i) {
            pool_vertices[i] = i;
        }
    }


    /* Split all vertices between the workers */
    subpool = (int*)malloc(sub_size*sizeof(int));
    MPI_Scatter(pool_vertices, sub_size, MPI_INT, 
            subpool, sub_size, MPI_INT, 0,
            MPI_COMM_WORLD);


    double diff = 0;
    struct timespec start, end;

    /* 
     * Benchmark running Dijkstra's Algorithm and calculating
     * all distances from vertex 1 on loaded graph
     */
    clock_gettime(CLOCK_MONOTONIC, &start);
    get_shortest_path(1, subpool, vertices, size, sub_size);
    clock_gettime(CLOCK_MONOTONIC, &end);


    diff = ((end.tv_sec - start.tv_sec) + (double)(end.tv_nsec - start.tv_nsec)/BILLION);
    if (mpi_rank == 0) {
        cout << "Time for graph " << file << " with " << world_size << " workers: " << diff << " s" << endl;
        free(pool_vertices);
    }

    free(subpool);
}
