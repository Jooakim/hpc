Assignment 4
===========

Enumerating
----------
Not much to add here, it works.



Dijkstra's Algorithm
--------------------

Nbr of workers      5,100   100,2000    100,10000   1000,10000
--------------      -----   --------    ---------   ----------
2                   0.0005s 0.19s       4.16s       9.18s
5                   0.001s  0.13s       2.54s       5.40s
10                  0.009s  0.19s       2.40s       4.65s

As we can see in the table above with small graphs the fewer the workers the faster, this is because the majority of the total time is communication between the processess. Therefor with few workers the communication takes less time which results in a faster run time. As we run with large graphs the majority of the time is spent in the second part of the while-loop, the distance calculation. For example, when running the largest graph, (1000,10000) with 2 workers only 0.6s of the total time is spent in the part between the while-loop condition and the second for loop for updating the distances. This equals only about 7% percent of the total time, which means that there is a lot of time to gain by adding more workers. With 10 workers 1.7s is spent in the first half of the program which amounts to 36%, it has increased. If we take a look at the time-gain from 2->5->10 workers we see that there is not such big difference between 5 and 10 workers. This means that the communication-time starts to outweigh the gain from adding more workers and it is reasonable to asume that the algorithm is closing in on it's peak run-rime.

On a side note: The implemented version of the parallellization of Dijsktra's Algorithm is not good at all for the current layout of the graph-structure. We can easily modify the second for-loop to achieve much faster resluts with only one worker, consider the following snippet: 
```c
for (pair<int,int> n : vertices[u]){
    if (!visited[n.first]) {
        int temp_dist = dist + n.second;
        if (temp_dist < local_dist[n.first]) {
            local_dist[n.first] = temp_dist;
        }
    }
}
```
Exchanging the second for loop with this would make the program run in under 2s with only one worker. Unfortunately I didn't have time to implement this in a parallell fashion.
