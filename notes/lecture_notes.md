Lecture Notes
=============


Lecture 1
---------

 - 3 types of lifetimes for data.
    * Execution time of program, the data remains in the memory the whole execution.
    * Time between explicit creation of data and explicit deletion of data. Heap allocation. malloc() ex.
    * During the execution of a function. Stack allocation, LIFO-structure, if one function is called from that function the data gets pushed onto the stack and when we return we pop the data from the stack


### During execution

 - Code (machine language)
 - Data (initialized and uninitialized)

Code and Data don't change in size while the program is executing

 - Heap (for dunamically allocated data)
 - Stack (for function local variables)

Heap and Stack change in size as program executes.

------------------------------------------------

TODO: Read up on Heap and Stack allocation

------------------------------------------------



Lecture 2
----------

IEEE Floating Point Standard: 

 - 32 bit value with 3 components (s, e, f)
    * s (1 bit sign)
    * e (8 bit exponent)
    * f (23 bit fraction)

Represents the value (-1)^s * 1.f * 2^(e-127)

### Computer organization

 - Main parts of a computer sustem:
    * Processor: Exectues programs
    * Main memory: Holds program and data
    * I/O devicxes: For communication with outside
 - Machine instruction: Description of primitive operation that machine hardware is able to execute e.g. ADD these two integers
 - Instruction Set: Complete specification of all the kinds of instruction that the processor hardware was built to execute.

The cpu, memory and i/o devices are connected through a Bus.

Lecture 3
----------
### Inside the CPU
 - Control harware: Hardware to manage instruction execution
 - ALU: Arithmetic and Logical Unit (hardware to do arithmetic, logical operations)
 - Registers: small units of memory to hold data/instructions temporarily during execution

#### Memory
 - What is memory?
    * Something that can remember things
 - There are different kinds of memory in a computer system
    * Some remember by the state an electrical circuit is in e.g. SRAM, Static RAM.
    * Other rememver by the amount of electrical chage stored in a capacitor e.g DRAM, Dynamic RAM - "Memory" in last lecture
    * Magnetic or optical properties e.g. regular hard drives, dvds.
 - They can vary greatly in their speed and capacity

#### Registers
 - There are 2 kinds of registers.
 - Special Purpose Registers
    * These are used for specific purposes by the control hardware.
    * Program Counter (PC): Used to remember the location in memory of the instruction currently being executed
    * Instruction Register (IR): used to remember that instruction.
    * Processor Status Register: used to remember status information about current state of processor , e.g., whether an arithmetic overflow has occurred
 - General Purpose Registers
    * Available for use by the programmer, the compiler knows about the GPR, so programs will use this.
    * Useful for remebering frequently used data
    * A typical processor today has 32 GPRs, say R0, R1,..., R31
    * The operands to an instruction could come either from registers or from main memory
    * Two types of GPR, Integer Registers and FP Registers

--------------------------------------------

##### Why use General Purpose Registers?
 - There is a large speed disparity between the processor (CPU) and the memory where instructions and data are stored
 - Consider a 1GHz processor
    * Corresponds to a 1 ns time scale
    * G (giga) 2^(30) for memory; 10^9 for frequency, disk size
 - Memory: ~ 100 ns time scale

The memory is much slower than the processor, which is why you want to use the general purpose register.

--------------------------------------------


#### Main Memory
 - Holds instructions and data
 - View it as a sequence of locations, each referred to by a unique memory address
 - If the size of each memory location is 1 Byte, we call the memory byte addressable
 - This is quite typical, as the smalles data (character) is represented in 1 Byte
 - Larger data items are stored in contiguous memory locations, e.g., a 4 Byte float would occupy 4 consucutive memory locations

##### Byte ordering

 - Two (main) types of byte ordering
 - Big-endian byte ordering
    * Uses the left-most address as the most significant byte, i.e. a 4 byte integer in memory 400-403 is read from left to right that is |&400|&401|&402|&403|.
 - Little-endian byte ordering
    * Uses the right-most address as the most significant byte.

Relevant if you have a program running on multiple processors.

-------------------------------------------

TODO: Write a program that checks if processor uses Big or Little endian

-------------------------------------------
