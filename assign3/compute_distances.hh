std::map<double, int> compute_distances(std::vector<struct point>, size_t);
std::map<double,int> compare_points(int, std::vector<struct point>, std::map<double,int>);
double calc_distance(struct point a, struct point b);

struct point {
    double x;
    double y;
    double z;
};
