#include <omp.h>
#include <map>
#include <vector>
#include <math.h>

#include "compute_distances.hh"

#define BILLION 1000000000L

using namespace std;


/*
 * Function for adding all occuring distances and number of occurances between
 * points in a vector. It does this by using openmp for parallelization,
 * where number of threads are specified in the function call. 
 */
map<double, int> compute_distances(vector<struct point> points, size_t n_threads) {
    map<double,int> global_distances;
    omp_set_num_threads(n_threads);
    size_t i;

    #pragma omp parallel shared(points) 
    {
        map<double, int> distances;
        #pragma omp for private(i) schedule(dynamic) 
        for (i = 0; i < points.size(); i++) {
            distances = compare_points(i, points, distances);
        }
        /* To avoid uneccesary copying of map */
        if (n_threads == 1) 
            global_distances = distances;
        else {
                /* Reduction of the thread-individual maps */
                #pragma omp critical
                {
                    for (auto it = distances.begin(); it != distances.end(); it++) {
                        auto temp_it = global_distances.find(it->first);
                        if (temp_it != global_distances.end()) {
                            temp_it->second = temp_it->second + it->second;
                        } else {
                            global_distances.insert({it->first, it->second});
                        }
                    }

                }
        }
    }
    #pragma omp barrier

    return global_distances;
}

/*
 * Function for comparing distance from a specific point
 * to all points onwards in the vector points.
 */
map<double,int> compare_points(int index, vector<struct point> points,  map<double,int> distances) { 
    double dist;

    struct point reference = points[index];

    size_t i;
    for (i = index + 1; i < points.size(); ++i) {
        dist = calc_distance(reference, points[i]);
        map<double,int>::iterator it;
        it = distances.find(dist);
        /*
         * If distance is already added, increment occurnaces
         * otherwise add the distance.
         */
        if (it != distances.end()) {
            it->second = it->second + 1;
        } else {
            distances.insert({dist, 1});
        }

    }
    return distances;
}

/*
 * Function to calculate the 3d-distance between two points rounded to
 * two decimals.
 */
double calc_distance(struct point a, struct point b) {
    double tot = sqrt(pow(a.x-b.x, 2) + pow(a.y-b.y,2) + pow(a.z-b.z,2));
    tot = round(100*tot)/100.0;
    return tot;
}
