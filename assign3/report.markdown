Assignment 3
============

 - Joakim Andersson
 - 920930-3594
 - joakande@student.chalmers.se
 - Chalmers

OpenMP
------

The benchmark of the program resulted in the following table:

Threads     Time
-------     ----
1           5.6s
2           3.0s
3           2.0s
4           1.6s


We see that the program performs better and better if we add more threads. The runtime is a near linear performance boost with an added thread, but not quite. This is partly because it takes more time to merge the different maps together in the critical section, since the statement blocks the threads from executing parallel, so the threads are put on stand-by. But it's also because it takes time for the scheduler to evenly distribute the for loop between the threads for equal run-time. I found that running with schedule(dynamic) was the fastest, and schedule(guided) the slowest.

One may think that the critical section is holding the performance back since the program cannot execute this in parallel, but if we take a look at the actual time spent in this section, this isn't the case.

### Four threads

Location            Time
--------            ----
Parallel section    1.64 seconds
Critical section    ~0.001 seconds

This is when adding together all four times for the critical section. The critical sections doesn't really have that big of an impact on the total run-time. But the program will flatten out and reach a point where adding more threads wouldn't improve the runtime. Testing the program on ozzy I found this number to be ~26-28 threads.

