#include <map>
#include <vector>
#include <fstream>

#include "compute_distances.hh"

using namespace std;

#define BILLION 1000000000L



int main(void) {
    vector<struct point> points;
    map<double,int> distances;
    static size_t n_threads;
    double diff, x, y, z;
    struct timespec start,end;

    /*
     * Read coordinates from file and insert into
     * points vector.
     */
    ifstream infile("cells.txt");
    while (infile >> x >> y >> z) {
        struct point temp;
        temp.x = x;
        temp.y = y;
        temp.z = z;
        points.push_back(temp);
    }

    /*
     * Loop to benchmark calculation of distances between
     * all points for different nbr of threads running 
     */
    for (n_threads = 1; n_threads < 5; n_threads++) {
        clock_gettime(CLOCK_MONOTONIC, &start);
        distances = compute_distances(points, n_threads);
        clock_gettime(CLOCK_MONOTONIC, &end);

        diff =  (end.tv_sec - start.tv_sec) + 
                (double)(end.tv_nsec - start.tv_nsec)/BILLION;

        printf ("%e seconds for %ld threads\n", diff, n_threads);
    }

    /*
     * Print the calculated distances as well as how
     * many occurances of each distance.
     */
    for (auto it = distances.begin(); it != distances.end(); it++) {
        printf ("%.2f \t %d\n", it->first, it->second);
    }

    return 0;
}



