#include "complex_prod.hh"
#include <vector>
#include <stdio.h>
#include <time.h>

using namespace std;

#define BILLION 1000000000L


int main (void) {
    const int SIZE = 30000;

    int total_iter = 20;


    double a_re[SIZE] = {};
    double a_im[SIZE] = {};
    double b_re[SIZE] = {};
    double b_im[SIZE] = {};
    double c_re[SIZE] = {};
    double c_im[SIZE] = {};

    int i;
    for(i = 0; i < SIZE; i++) {
        b_re[i] = i;
        b_im[i] = i;
        c_re[i] = i;
        c_im[i] = i;
    }

    double diff = 0;
    struct timespec start, end;

    int j;
    for (i = 0; i < total_iter; i++) {
        /* measure time w/o inlining */
        clock_gettime(CLOCK_MONOTONIC, &start); /* mark start time */ 

        for (j = 0; j < SIZE; j++) {
            mul_cpx(&a_re[i],&a_im[i],&b_re[i],&b_im[i],&c_re[i],&c_im[i]);
        }

        clock_gettime(CLOCK_MONOTONIC, &end); /* mark the end time */ 

        diff += (((end.tv_sec - start.tv_sec) + (double)(end.tv_nsec - start.tv_nsec))/BILLION);
    }
    printf ("Time elapsed: \t\t\t %.6f seconds \n", diff);

    diff = 0;

    for (i = 0; i < total_iter; i++) {
        /* measure time with inlining */
        clock_gettime(CLOCK_MONOTONIC, &start); /* mark start time */ 

        int ix;
        for (ix = 0; ix < SIZE; ix++) {
            a_re[ix] = b_re[ix]*c_re[ix] - b_im[ix]*c_im[ix];
            a_im[ix] = b_re[ix]*c_im[ix] + c_re[ix]*b_im[ix];
        }
        //mul_cpx_fast(a_re,a_im,b_re,b_im,c_re,c_im);

        clock_gettime(CLOCK_MONOTONIC, &end); /* mark the end time */ 

        diff += (((end.tv_sec - start.tv_sec) + (double)(end.tv_nsec - start.tv_nsec))/BILLION);
    }
    printf ("Time elapsed with inlined: \t %.6f seconds \n", diff/total_iter);


}
