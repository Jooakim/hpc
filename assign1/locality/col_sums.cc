#include <vector>

#include "col_sums.hh"

using namespace std;

vector<double>
col_sums( vector< vector<double> > matrix ) {
  size_t nrs = matrix[0].size();
  size_t ncs = matrix.size();

  vector<double> sums;
  sums.reserve(nrs);

  for ( size_t jx=0; jx < ncs; ++jx ) {
    double sum = 0;
    for ( size_t ix=0; ix < nrs; ++ix )
      sum += matrix[ix][jx];
      sums.push_back(sum);
  }

  return sums;
}

