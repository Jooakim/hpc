#include <iostream>
#include <vector>
#include <time.h>
#include <stdio.h>

#include "row_sums.hh"
#include "col_sums.hh"
#include "col_sums_faster.hh"

using namespace std;


#define BILLION 1000000000L

vector<double> col_sums( vector< vector<double> > matrix );


int SIZE = 1000;
int NBR_OF_ITERATIONS = 50;

int main(void) {
    /* Create matrix */
    vector< vector<double> > matrix(SIZE, vector<double>(SIZE,10));
    vector< vector<double> > matrix2(SIZE, vector<double>(SIZE,10));
    vector< vector<double> > matrix3(SIZE, vector<double>(SIZE,10));


    double diff;
    struct timespec start, end;
    
    /* Row summation benchmarking */
    int i;
    /* measure time*/
    clock_gettime(CLOCK_MONOTONIC, &start); /* mark start time */ 
    for (int i = 0; i < NBR_OF_ITERATIONS; i++) {
        vector<double> rowSums = row_sums(matrix);
    }

    clock_gettime(CLOCK_MONOTONIC, &end); /* mark the end time */ 

    diff = (double) ((BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec));
    printf ("Average time for row summation: \t %g nanoseconds \n", diff/NBR_OF_ITERATIONS);


    /* Column summation benchmarking */
    /* measure time*/
    clock_gettime(CLOCK_MONOTONIC, &start); /* mark start time */ 
    for (int i = 0; i < NBR_OF_ITERATIONS; i++) {
        vector<double> colSums = col_sums(matrix2);
    }

    clock_gettime(CLOCK_MONOTONIC, &end); /* mark the end time */ 

    diff = (double) ((BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec));

    printf ("Average time for col summation: \t %g nanoseconds \n", diff/NBR_OF_ITERATIONS);
    
    /* Column summation benchmarking with better locality */
    /* measure time*/
    clock_gettime(CLOCK_MONOTONIC, &start); /* mark start time */ 
    for (int i = 0; i < NBR_OF_ITERATIONS; i++) {
        vector<double> colSums = col_sums_faster(matrix3);
    }

    clock_gettime(CLOCK_MONOTONIC, &end); /* mark the end time */ 

    diff = (double) ((BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec));

    printf ("Average time for faster col summation: \t %g nanoseconds \n", diff/NBR_OF_ITERATIONS);

    return 0;
}


