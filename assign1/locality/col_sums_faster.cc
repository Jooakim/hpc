#include <vector>

#include "col_sums_faster.hh"

using namespace std;

vector<double>
col_sums_faster( vector< vector<double> > matrix ) {
  size_t nrs = matrix[0].size();
  size_t ncs = matrix.size();

  vector<double> sums;
  sums.reserve(nrs);

  for ( size_t ix=0; ix < ncs; ++ix ) {
    for ( size_t jx=0; jx < nrs; ++jx )
      sums[jx] += matrix[ix][jx];
  }
  return sums;
}
