double dummy_operations(int nbr_of_iterations, int * a, int * b);
double add_floats(float *, int, int *, int * );
double add_doubles(double *, int, int *, int *);
double add_long_ints(long int *, int, int *, int *);
