#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "addition.h"
#include "add_multi.h"
#include "division.h"
#include "sin.h"
#include "exp.h"


void benchmark_addition(int);
void benchmark_add_multi(int);
void benchmark_division(int);
void benchmark_sinus(int);
void benchmark_exp(int);


#define BILLION 1000000000L



int main (void) {

    int nbr_of_iterations = 3e8;

    printf("-----------------Addition------------------\n");
    benchmark_addition(nbr_of_iterations);
    
    printf ("\n");

    printf("----------Addition-Multiplication----------\n");
    benchmark_add_multi(nbr_of_iterations);
    
    printf ("\n");

    printf("-----------------Division------------------\n");
    benchmark_division(nbr_of_iterations);
    
    printf ("\n");

    printf("------------------Sinus--------------------\n");
    benchmark_sinus(1e5);

    printf("---------------Exponential-----------------\n");
    benchmark_exp(1e5);
    return 0;

}


void benchmark_addition(int nbr_of_iterations) {
    float * f1 = (float *) malloc(nbr_of_iterations*sizeof(float*));

    double * d1 = (double *) malloc(nbr_of_iterations*sizeof(double *));

    long int * li1 = (long int*) malloc(nbr_of_iterations*sizeof(long int *));

    int i;
    for (i = 0; i < nbr_of_iterations; i++) {
        f1[i] = (float)rand()/10000;
        d1[i] = (double)rand()/10000;
        li1[i] = (long int)rand()/10000;
    }

    double time;

    int a = 1;
    int b = 1;

    /* Addition of floats */
    time = add_floats(f1,nbr_of_iterations, &a, &b);
    printf ("Time for adding two floats: \t \t %f nanoseconds \n", time);
    printf ("Resulting OPS: \t \t \t \t %e \n", BILLION/time);
    a = 1;
    b = 1;

    /* Addition of doubles */
    time = add_doubles(d1, nbr_of_iterations, &a, &b);
    printf ("Time for adding two doubles: \t \t %f nanoseconds \n", time);
    printf ("Resulting OPS: \t \t \t \t %e \n", BILLION/time);
    a = 1;
    b = 1;

    /* Addtion of long ints */
    time = add_long_ints(li1,nbr_of_iterations, &a, &b);
    printf ("Time for adding two long ints: \t \t %f nanoseconds \n", time);
    printf ("Resulting OPS: \t \t \t \t %e \n", BILLION/time);

    free(f1); free(d1); free(li1);
}


void benchmark_add_multi(int nbr_of_iterations) {
    float * f1 = (float *) malloc(nbr_of_iterations*sizeof(float*));

    double * d1 = (double *) malloc(nbr_of_iterations*sizeof(double *));

    long int * li1 = (long int*) malloc(nbr_of_iterations*sizeof(long int *));

    int i;
    for (i = 0; i < nbr_of_iterations; i++) {
        f1[i] = (float)rand()/10000;
        d1[i] = (double)rand()/10000;
        li1[i] = (long int)rand()/10000;
    }

    double time;
    int a = 1;
    int b = 1;
    /* Addition-multiplication of floats  */
    time = add_multi_floats(f1,nbr_of_iterations, &a, &b);
    printf ("Time for add-multi two floats:  \t %f nanoseconds \n", time);
    printf ("Resulting OPS: \t \t \t \t %e \n", BILLION/time);

    a = 1;
    b = 1;
    /* Addition-multiplication of doubles */
    time = add_multi_doubles(d1,nbr_of_iterations, &a, &b);
    printf ("Time for add-multi two doubles: \t %f nanoseconds \n", time);
    printf ("Resulting OPS: \t \t \t \t %e \n", BILLION/time);

    a = 1;
    b = 1;
    /* Addtion-multiplication of long ints */
    time = add_multi_long_ints(li1,nbr_of_iterations, &a, &b);
    printf ("Time for add-multi two long ints: \t %f nanoseconds \n", time);
    printf ("Resulting OPS: \t \t \t \t %e \n", BILLION/time);

    free(f1); free(d1); free(li1);
}


void benchmark_division(int nbr_of_iterations) {
    float * f1 = (float *) malloc(nbr_of_iterations*sizeof(float*));

    double * d1 = (double *) malloc(nbr_of_iterations*sizeof(double *));

    long int * li1 = (long int*) malloc(nbr_of_iterations*sizeof(long int *));

    int i;
    for (i = 0; i < nbr_of_iterations; i++) {
        f1[i] = (float)rand()/10000 + 1;
        d1[i] = (double)rand()/10000 + 1;
        li1[i] = (long int)rand()/10000 + 1;
    }

    double time;
    int a = 1;
    int b = 1;

    /* Division of floats */
    time = divide_floats(f1, nbr_of_iterations, &a, &b);
    printf ("Time for division of two floats: \t %f nanoseconds \n", time);
    printf ("Resulting OPS: \t \t \t \t %e \n", BILLION/time);
    

    a = 1;
    b = 1;
    /* Division of doubles */

    divide_doubles(d1, nbr_of_iterations, &a, &b);
    printf ("Time for division of two double: \t %f nanoseconds \n", time);
    printf ("Resulting OPS: \t \t \t \t %e \n", BILLION/time);

    a = 1;
    b = 1;
    /* Division of long ints*/
    time = divide_long_ints(li1, nbr_of_iterations, &a, &b);
    printf ("Time for division of two long int: \t %f nanoseconds \n", time);
    printf ("Resulting OPS: \t \t \t \t %e \n", BILLION/time);
    
    free(f1); free(d1); free(li1);

}


void benchmark_sinus(int nbr_of_iterations) {
    float * f1 = (float *) malloc(nbr_of_iterations*sizeof(float*));

    double * d1 = (double *) malloc(nbr_of_iterations*sizeof(double *));

    int i;
    for (i = 0; i < nbr_of_iterations; i++) {
        f1[i] = (float)rand()/10000;
        d1[i] = (double)rand()/10000;
    }

    double time;
    int a = 1;
    int b = 1;

    /* Sinus of float */
    time = sin_float(f1, nbr_of_iterations, &a, &b);
    printf ("Time for sinus of two floats: \t\t %f nanoseconds \n", time);
    printf ("Resulting OPS: \t \t \t \t %e \n", BILLION/time);

    a = 1;
    b = 1;
    /* Sinus of double */
    time = sin_double(d1, nbr_of_iterations, &a, &b);
    printf ("Time for sinus of two double: \t\t %f nanoseconds \n", time);
    printf ("Resulting OPS: \t \t \t \t %e \n", BILLION/time);

    free(f1); free(d1);
}

void benchmark_exp(int nbr_of_iterations) {
    float * f1 = (float *) malloc(nbr_of_iterations*sizeof(float*));

    double * d1 = (double *) malloc(nbr_of_iterations*sizeof(double *));

    int i;
    for (i = 0; i < nbr_of_iterations; i++) {
        f1[i] = (float)rand()/10000;
        d1[i] = (double)rand()/10000;
    }
    double time;
    int a = 1;
    int b = 1;

    /* Exponential of float */
    time = exp_float(f1, nbr_of_iterations, &a, &b);
    printf ("Time for exponential of two floats: \t %f nanoseconds \n", time);
    printf ("Resulting OPS: \t \t \t \t %e \n", BILLION/time);

    a = 1;
    b = 1;
    /* Exponential of double */
    time = exp_double(d1, nbr_of_iterations, &a, &b);
    printf ("Time for exponential of two double: \t %f nanoseconds \n", time);
    printf ("Resulting OPS: \t \t \t \t %e \n", BILLION/time);

    free(f1); free(d1);

}
