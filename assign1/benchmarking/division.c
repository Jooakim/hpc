#include "division.h"
#include "addition.h"
#include <time.h> 
#include <stdio.h>

#define BILLION 1000000000L

double divide_floats(float * f1, int nbr_of_iterations, int * a, int * b) {
    double diff;
    struct timespec start, end;

    /* measure time */ 
    int i;
    clock_gettime(CLOCK_MONOTONIC, &start); /* mark start time */ 
    for (i = 0; i < nbr_of_iterations; i++) {
        *b = *b + *a;
        *b = *b + *a;
        f1[i] /= f1[i];
    }
    clock_gettime(CLOCK_MONOTONIC, &end); /* mark the end time */ 

    diff = (double) ((BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec));

    /*clock_gettime(CLOCK_MONOTONIC, &start); [> mark start time <] */
    /*for (i = 0; i < nbr_of_iterations; i++) {*/
        /*f1[i] = f1[i];*/
    /*}*/
    /*clock_gettime(CLOCK_MONOTONIC, &end); [> mark the end time <] */

    /*diff -= (double) ((BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec));*/

    diff -= dummy_operations(nbr_of_iterations, a, b);


    return diff/nbr_of_iterations;

}

double divide_doubles(double * d1, int nbr_of_iterations, int * a, int * b) {
    double diff;
    struct timespec start, end;

    /* measure time */ 
    int i;
    clock_gettime(CLOCK_MONOTONIC, &start); /* mark start time */ 
    for (i = 0; i < nbr_of_iterations; i++) {
        *b = *b + *a;
        *b = *b + *a;
        d1[i] /= d1[i];
    }


    clock_gettime(CLOCK_MONOTONIC, &end); /* mark the end time */ 

    diff = (double) ((BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec));

    /*clock_gettime(CLOCK_MONOTONIC, &start); [> mark start time <] */
    /*for (i = 0; i < nbr_of_iterations; i++) {*/
        /*d1[i]  = d1[i];*/
    /*}*/
    /*clock_gettime(CLOCK_MONOTONIC, &end); [> mark the end time <] */

    /*diff -= (double) ((BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec));*/

    diff -= dummy_operations(nbr_of_iterations, a, b);
    return diff/nbr_of_iterations;

}

double divide_long_ints(long int * li1, int nbr_of_iterations, int * a, int * b) {
    double diff;
    struct timespec start, end;

    /* measure time */ 
    int i;
    clock_gettime(CLOCK_MONOTONIC, &start); /* mark start time */ 
    for (i = 0; i < nbr_of_iterations; i++) {
        *b = *b + *a;
        *b = *b + *a;
        li1[i] /= li1[i];

    }

    clock_gettime(CLOCK_MONOTONIC, &end); /* mark the end time */ 

    diff = (double) ((BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec));


    /*clock_gettime(CLOCK_MONOTONIC, &start); [> mark start time <] */
    /*for (i = 0; i < nbr_of_iterations; i++) {*/
        /*li1[i] = li1[i];*/
    /*}*/

    /*diff -= (double) ((BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec));*/
    diff -= dummy_operations(nbr_of_iterations, a, b);

    return diff/nbr_of_iterations;

}
