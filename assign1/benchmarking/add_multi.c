#include "add_multi.h"
#include "addition.h"
#include <time.h> 
#include <stdio.h>


#define BILLION 1000000000L

double add_multi_floats(float * f1, int nbr_of_iterations, int * a, int * b) {
    double diff;
    struct timespec start, end;
    float temp = 1;

    /* measure time*/
    clock_gettime(CLOCK_MONOTONIC, &start); /* mark start time */ 
    int i;
    for (i = 0; i < nbr_of_iterations; i++) {
        *b = *b + *a;
        *b = *b + *a;
        f1[i] += f1[i]*f1[i];
    }

    clock_gettime(CLOCK_MONOTONIC, &end); /* mark the end time */ 


    diff = (double) ((BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec));
    /*diff = (double) ((BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec));*/
    /*clock_gettime(CLOCK_MONOTONIC, &start); [> mark start time <] */
    /*for (i = 0; i < nbr_of_iterations; i++) {*/
        /*temp = f1[i];*/
        /*temp = f1[i];*/
    /*}*/
    /*clock_gettime(CLOCK_MONOTONIC, &end); [> mark the end time <] */

    /*diff -= (double) ((BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec));*/

    diff -= dummy_operations(nbr_of_iterations, a, b);


    return diff/nbr_of_iterations;

}

double add_multi_doubles(double * d1, int nbr_of_iterations, int * a, int * b) {
    double diff;
    struct timespec start, end;
    double temp = 1;

    /* measure time*/
    int i;
    clock_gettime(CLOCK_MONOTONIC, &start); /* mark start time */ 
    for (i = 0; i < nbr_of_iterations; i++) {
        *b = *b + *a;
        *b = *b + *a;
        d1[i] += d1[i]*d1[i];
    }
    clock_gettime(CLOCK_MONOTONIC, &end); /* mark the end time */ 
    diff = (double) ((BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec));

    /*clock_gettime(CLOCK_MONOTONIC, &start); [> mark start time <] */
    /*for (i = 0; i < nbr_of_iterations; i++) {*/
        /*temp = d1[i];*/
        /*temp = d1[i];*/
    /*}*/

    /*clock_gettime(CLOCK_MONOTONIC, &end); [> mark the end time <] */
    /*diff -= (double) ((BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec));*/

    diff -= dummy_operations(nbr_of_iterations, a, b);
    return diff/nbr_of_iterations;

}

double add_multi_long_ints(long int * li1, int nbr_of_iterations, int * a, int * b) {

    double diff;
    struct timespec start, end;
    long int temp = 1;

    /* measure time */ 
    int i;
    clock_gettime(CLOCK_MONOTONIC, &start); /* mark start time */ 
    for (i = 0; i < nbr_of_iterations; i++) {
        *b = *b + *a;
        *b = *b + *a;
        li1[i] += li1[i]*li1[i];
    }

    clock_gettime(CLOCK_MONOTONIC, &end); /* mark the end time */ 


    diff = (double) ((BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec));
    /*clock_gettime(CLOCK_MONOTONIC, &start); [> mark start time <] */
    /*for (i = 0; i < nbr_of_iterations; i++) {*/
        /*temp = li1[i];*/
        /*temp = li1[i];*/
    /*}*/

    /*clock_gettime(CLOCK_MONOTONIC, &end); [> mark the end time <] */
    /*diff -= (double) ((BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec));*/

    diff -= dummy_operations(nbr_of_iterations, a, b);
    

    return diff/nbr_of_iterations;
}
