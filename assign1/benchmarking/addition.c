#include <stdio.h>
#include "addition.h"
#include <time.h> 


#define BILLION 1000000000L

double dummy_operations(int nbr_of_iterations, int * a, int * b) {
    double diff = 0;
    struct timespec start, end;
    
    clock_gettime(CLOCK_MONOTONIC, &start);
    int i;
    for (i = 0; i < nbr_of_iterations; i++) {
        *b = *b + *a;
    }

    clock_gettime(CLOCK_MONOTONIC, &end);

    diff = (double) ((BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec));

    /*printf ("Value of b dummy: %d\n", b);*/
    /*printf ("Time of dummy: %f\n", diff);*/

    return diff;
}

double add_floats(float * f1, int nbr_of_iterations, int * a, int * b) {
    double diff = 0;
    struct timespec start, end;
    float * temp = (float *) a;

    /* measure time*/
    int i;
    clock_gettime(CLOCK_MONOTONIC, &start); /* mark start time */ 
    for (i = 0; i < nbr_of_iterations; i++) {
        *b = *b + *a;
        f1[i] = f1[i] + f1[i];
    }
    clock_gettime(CLOCK_MONOTONIC, &end); /* mark the end time */ 
    diff = (double) ((BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec));

    /*printf ("Time of float: %f\n", diff);*/

    diff -= dummy_operations(nbr_of_iterations, a, b);

    /*printf ("Value of additions: %f\n", f1);*/
    /*printf ("Value of b: %d\n", *b);*/

    return diff/nbr_of_iterations;
}

double add_doubles(double * d1, int nbr_of_iterations, int * a, int * b) {
    double diff = 0;
    struct timespec start, end;
    double * temp = (double *) a;

    /* measure time*/
    int i;
    clock_gettime(CLOCK_MONOTONIC, &start); /* mark start time */ 
    for (i = 0; i < nbr_of_iterations; i++) {
        *b = *b + *a;
        d1[i] = d1[i] + d1[i];
    }
    clock_gettime(CLOCK_MONOTONIC, &end); /* mark the end time */ 
    diff = (double) ((BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec));
    /*printf ("Time of float: %f\n", diff);*/

    diff -= dummy_operations(nbr_of_iterations, a, b);
    /*printf ("Value of additions: %f\n", d1);*/
    /*printf ("Value of b: %d\n", *b);*/


    return diff/nbr_of_iterations;
}

double add_long_ints(long int * li1, int nbr_of_iterations, int * a, int * b) {
    double diff;
    struct timespec start, end;
    long int * temp = (long int *) a;

    /* measure time */ 
    int i;
    clock_gettime(CLOCK_MONOTONIC, &start); /* mark start time */ 
    for (i = 0; i < nbr_of_iterations; i++) {
        *b = *b + *a;
        li1[i] = li1[i] + li1[i];
    }
    clock_gettime(CLOCK_MONOTONIC, &end); /* mark the end time */ 
    diff = (double) ((BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec));

    diff -= dummy_operations(nbr_of_iterations, a, b);

    /*printf ("Value of additions: %ld\n", li1);*/
    /*printf ("Value of b: %d\n", *b);*/

    return diff/nbr_of_iterations;
}

