#include <vector>
#include <stdio.h>
#include <time.h>

#include "wo_indirect_addressing.hh"

using namespace std;

#define BILLION 1000000000L

void benchmark_no_ia(vector<int> * x, vector<int> * y){
    double diff;
    struct timespec start, end;
    size_t kx;
    int n = (*y).size();
    int a = 2;

    clock_gettime(CLOCK_MONOTONIC, &start); /* mark start time */ 
    
    for (kx=0; kx < n; ++kx) {
        (*y)[kx] += a * (*x)[kx];
    }
    
    clock_gettime(CLOCK_MONOTONIC, &end); /* mark end time */

    diff = (double) ((BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec));

    printf ("Time taken w/o indirect addressing: \t %f nanoseconds \n", diff);

}
