#include <vector>

#include "first_version.hh"
#include "second_version.hh"
#include "wo_indirect_addressing.hh"

using namespace std;


#define BILLION 1000000000L

int main(void) { 
    int n = 1000000, m = 1000, a = 2;

    vector<int> p;
    p.reserve(n);

    /* Create vectors */

    vector<int> x(n,10);
    vector<int> y(n,12);

    int ix = 0, jx, kx;;
    for (jx=0; jx < m; ++jx) {
        for (kx=0; kx < m; ++kx) {
            p[jx + m*kx] = ix++;
        }
    }


    benchmark_first_version(&x, &y, &p);



    /* Create p according to second method */
    for (ix=0; ix < n; ++ix) {
          p[ix] = ix;
    }

    /* Benchmark second version of p*/
    benchmark_second_version(&x, &y, &p);

    benchmark_no_ia(&x, &y);


    return 0;
}



