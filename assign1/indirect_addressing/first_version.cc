#include <time.h>
#include <stdio.h>
#include <vector>


#include "first_version.hh"

#define BILLION 1000000000L

using namespace std;

void benchmark_first_version(vector<int> * x, vector<int> * y, vector<int> * p) {
    /* Benchmark first version of p*/
    double diff;
    struct timespec start, end;
    size_t kx, jx;
    int a = 2;
    int n = (*y).size();

    clock_gettime(CLOCK_MONOTONIC, &start); /* mark start time */ 
    
    for (kx=0; kx < n; ++kx) {
        jx = (*p)[kx];
        (*y)[jx] += a * (*x)[jx];
    }
    
    clock_gettime(CLOCK_MONOTONIC, &end); /* mark end time */

    diff = (double) ((BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec));

    printf ("Time taken first version: \t \t %f nanoseconds \n", diff);
}
