#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memset */
#include <math.h>

double dot(double *, double *, size_t);
void profile_implementation(size_t);

int main(void) {

    
    profile_implementation(1000);

    return 0;
    
}

double dot(double * x, double * y, size_t jx) {
    double sum = 0;
    size_t ix;
    for (ix = 0; ix < jx; ++ix)
        sum += x[ix] * y[ix];
    return sum;
}


void profile_implementation(size_t n) {

    
    /* Create matrix */
    double  **A = malloc(n * sizeof *A);
    size_t ix, jx;
    for (ix = 0; ix < n; ix++) {
        A[ix] = malloc(n * sizeof *A[ix]);
        A[ix][ix] = 1;
    }


    

    for (jx = 0; jx < n; ++jx) {
        double row_sum = 0;
        for (ix = 0; ix < jx; ++ix ) {
            row_sum += A[jx][ix] * A[jx][ix];
        }
        A[jx][jx] = sqrt(A[jx][jx] - row_sum);

        for (ix = jx+1; ix < n; ++ix)  {
            A[ix][jx] = ( A[ix][jx] - dot(A[ix], A[jx], jx) ) / A[jx][jx];
        }
    }


    /* Free the allocated memory */
    free(A);
    return;

}
