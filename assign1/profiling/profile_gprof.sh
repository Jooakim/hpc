#!/bin/bash

# Build
make

# Run
./profiling

gprof profiling gmon.out > analysis.txt
gcov profiling 

# Convert 
gprof2dot analysis.txt > call_graph.dot

# Visualize
xdot call_graph.dot

# Save graph as png
dot -Tpng call_graph.dot -o ../material/call_graph.png
