Assignment 1: Optimization
============

 - Joakim Andersson
 - 920930-3594
 - Chalmers

Benchmarking
------------

### Ozzy
 - Architecture: x86_64
 - OS: Ubuntu 14.04.3 LTS
 - Processor: Intel(R) Xeon(R) CPU E5-2683 v3 @ 2.00 GHz
 - Flags: -O2

Operation                   Type        Time (ns)   OPS
------------------------    -----       ------      -----
Addition                    Float       0.34        2.937e9
Addition                    Double      0.66        1.525e9
Addition                    Long int    0.65        1.548e9
Addition-Multiplication     Float       0.63        1.579e9
Addition-Multiplication     Double      0.99        1.006e9
Addition-Multiplication     Long int    0.95        1.056e9
Division                    Float       1.90        5.272e8
Division                    Double      1.90        5.272e8
Division                    Long int    9.90        1.010e8
Sinus                       Float       12.82       7.800e7
Sinus                       Double      41.50       2.410e7
Exponential                 Float       10.48       9.540e7
Exponential                 Double      11.69       8.555e7 

From the table above we see that all operations operates the fastest with floats. This follows what we would expect as float-types are represented with the least amount of memory. On my system, a float is 4 bytes wheras both double and long int is 8 byte.

From the specs of the processor used we know that it has a turbo-boost function which maxes out at 3.00 GHz. This means from the above table that the fastest operation of them all is addition of floats, as this only takes one cycle. Addition with double and long int take 2 cycles, as does add-multiply for floats. Add-multiply takes 3 cycles for double and long int. Division is equally fast with floats and doubles, 6 cycles. As it turns out division is very slow for long ints, 30 cycles, which is intresting. Cycles for sinus and exponential gets a little harder to determine but sinus: float ~38 cycles, double ~120 cycles. Exponential: float ~31 cycles, double ~35 cycles.

Locality
------------

The times for different optimization flags is shown below:

Function        O0      O2      O3      Ofast
--------        --      --      --      -----
row_sum         6.79e6  6.82e6  6.74e6  6.83e6
col_sum         1.35e7  1.34e7  1.49e7  1.49e7
col_sum_fast    8.25e6  8.35e6  8.37e6  8.35e6

As we can se, the optimization flag doesn't have any real affect on the code, the compiler doesn't make the necessary changes to make col_sum faster, we have to do this ourselves. Since all the times are abaout the same we can conclude that the limiting factor is reading from memory.

The reason why the first implementation of col_sum is slower than row_sum is because it has bad locality. We have to load a different memory each time we iterate the loops. By instead allowing each element in the inner vector increment a new vector each loop we can achieve almost the same result as for the row_sum function.


Indirect Addressing
------------

Below the output of the execution is found

```
Time taken first version:               9355729 nanoseconds 
Time taken second version:              6195809 nanoseconds 
Time taken w/o indirect addressing:     4998114 nanoseconds 
```

As we excpect direct addressing is much faster than indirect addressing. This is partly because we have to look up one extra variable in order to access the variable for multiplication. But the main reason is because in order to access the two variables we have to load cache into the cpu, since the two are not in the same register. 

The difference between the two slower methods are how we save the index in the vector p. The second version uses a very straight forward method where we don't have to load so much new information each loop since we can read in a block and then keep that block for multiple iterations, so the main time difference between the second and third version is accessing two variable rather than one. The First version on the other hand takes a lot longer since the indices in p are not loaded in a fast way, we have to reload cache much more often than in the other two, so a lot of time goes to loading cache.

Inlining
------------
Below we see the output of the program:


Type        Time (s)
----        ------------ 
Function    0.003683
Inline      0.000095


We note that the inlined version is much faster (~x35) than the function. The reason for this is that calling functions is time consuming in it self. In order to execute instruction for a called function we must:


 1. Push arguments to the stack
 2. Change instruction block
 3. Return after the function is done.

 The above is not necessary if the code is inlined which is why the inlined code is much faster.


LAPack
------------
In the image below a benchmark of the different Cholesky implementations is presented.

<img src="material/time_plot.png" width="500">

From the picture above we see that both the GSL and PLASMA implementations are faster than our naive implementation. Whether it is the GSL or PLASMA-implementation that is the fastest is dependant upon the system it is installed on. This is because PLASMA is dependant on the BLAS library which can be compiled specifically for the system or a standard variant which may be very slow. Although PLASMA is still a much better implementation than our own since it can utilize more cores. In the picture above the program was run on ozzy and as we can see the PLASMA implementation performs better than GSL but not with a large margin. This is probably because the standard BLAS library is not very optimized for ozzy. Because with an optimized BLAS we would expect a more significant difference.


Profiling
------------

From the output of the call graph in gprof we can read the following:

index   % time      self    children    called          name
-----   ------      ----    --------    ------          -----
                    0.01    0.36        1/1             main [2]
[1]     100.0       0.01    0.36        1               profile_implementation [1]
                    0.36    0.00        499500/499500   dot [3]


displayed as a graph below.

![](material/call_graph.png "Call graph")

The most time consuming function is the dot-function in the implementation as this equals 36/37 of the total run-time. So the for loop itself that calls on the dot-function in profile_implementation has a very little effect on the total execution time. 

By using gcov we identify the most called lines to be:

```
   499500:   18:double dot(double * x, double * y, size_t jx) {
   499500:   19:    double sum = 0;
166666500:   21:    for (ix = 0; ix < jx; ++ix)
166167000:   22:        sum += x[ix] * y[ix];
   499500:   23:    return sum;
        -:   24:}
     1001:   41:    for (jx = 0; jx < n; ++jx) {
     1000:   42:        double row_sum = 0;
   500500:   43:        for (ix = 0; ix < jx; ++ix ) {
   499500:   44:            row_sum += A[jx][ix] * A[jx][ix];
        -:   45:        }
     1000:   46:        A[jx][jx] = sqrt(A[jx][jx] - row_sum);
        -:   47:
   500500:   48:        for (ix = jx+1; ix < n; ++ix)  {
   499500:   49:            A[ix][jx] = ( A[ix][jx] - dot(A[ix], A[jx], jx) ) / A[jx][jx];
        -:   50:        }
        -:   51:    }
```

where the line 21 and 22 is the by far most called lines. This results in that the limiting factor of the implementation is the add-multiply operation since it is called 300 times as often as any other line.

Perhaps you would suspect the line 46 to be the most timeconsuming as this calls the math-function sqrt. But as we see in the gcov inspection this only gets called 1000 times. In order for that line to be more time-consuming than 21-22, the sqrt-function would have to be about x100000 slower than the add-multiply operation. From our benchmark of the two function sin and exp we see that that isn't the case.





