clf
clc

impl = csvread('implementation_times.txt');
gnu = csvread('gnu_times.txt');
plasma = csvread('plasma_times.txt');

hold on
plot(impl(:,1), impl(:,2),'-o');
plot(gnu(:,1), gnu(:,2),'--o');
plot(plasma(:,1), plasma(:,2),'-.o');
legend('impl','gnu','plasma','Location', 'northwest')
xlabel('Matrix size');
ylabel('Time, s');
print('../material/time_plot', '-dpng');
