#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memset */
#include <math.h>

#include "naive_implementation.h"
#include "gnu_implementation.h"
#include "plasma_implementation.h"


int main(void) {


    FILE *fimple, *fgnu, *fplasma;

    fimple = fopen("implementation_times.txt", "w+");
    fgnu = fopen("gnu_times.txt", "w+");
    fplasma = fopen("plasma_times.txt", "w+");


    int total_iter = 20;
    size_t n;
    for (n = 50; n <= 1500; n += 50) {
        fprintf(fimple, "%ld, %.6f \n",n, benchmark_implementation(n, total_iter));
        fprintf(fgnu, "%ld, %.6f \n",n, benchmark_gnu(n, total_iter));
        fprintf(fplasma, "%ld, %.6f \n",n, benchmark_plasma(n, total_iter));
    }

    fclose(fimple);
    fclose(fgnu);
    fclose(fplasma);
    return 0;

}








