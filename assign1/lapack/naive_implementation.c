#include <stdio.h> 
#include <time.h>
#include <stdlib.h>
#include <math.h>

#include "naive_implementation.h"

#define BILLION 1000000000L


double dot(double * x, double * y, size_t jx) {
    double sum = 0;
    size_t ix;
    for (ix = 0; ix < jx; ++ix)
        sum += x[ix] * y[ix];
    return sum;
}

double benchmark_implementation(size_t n, int total_iter) {

    /* Benchmark */
    double diff = 0;
    struct timespec start, end;

    int iter;
    for (iter = 0; iter < total_iter; iter++) {
        /* Create matrix */
        double  **A = malloc(n * sizeof *A);
        size_t ix, jx;
        for (ix = 0; ix < n; ix++) {
            A[ix] = malloc(n * sizeof *A[ix]);
            A[ix][ix] = 1;
        }






        clock_gettime(CLOCK_MONOTONIC, &start); /* mark start time */ 
        for (jx = 0; jx < n; ++jx) {
            double row_sum = 0;
            for (ix = 0; ix < jx; ++ix ) {
                row_sum += A[jx][ix] * A[jx][ix];
            }
            A[jx][jx] = sqrt(A[jx][jx] - row_sum);

            for (ix = jx+1; ix < n; ++ix)  {
                A[ix][jx] = ( A[ix][jx] - dot(A[ix], A[jx], jx) ) / A[jx][jx];
            }
        }

        clock_gettime(CLOCK_MONOTONIC, &end); /* mark the end time */ 
        free(A);

        /* Difference in seconds */
        diff += (((end.tv_sec - start.tv_sec) + (double)(end.tv_nsec - start.tv_nsec)/BILLION));
    }
    printf ("Time of decomposition with size %ld :\t \t \t %g seconds \n", n, diff);


    /* Free the allocated memory */

    return diff/total_iter;

}
