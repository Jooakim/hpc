#include <stdio.h>
#include <plasma.h>
#include <cblas.h>
#include <lapacke.h>
#include <core_blas.h>
#include <quark.h>
#include <hwloc.h>
#include <time.h>

#include "plasma_implementation.h"

#define BILLION 1000000000L

double benchmark_plasma(size_t n, int total_iter) {

    /* Benchmark */
    double diff = 0;
    struct timespec start, end;

    int cores = 3;
    int N     = n;
    int LDA   = n;
    int NRHS  = 1;
    int LDB   = n;
    int info;

    /* Plasma Initialize */
    PLASMA_Init(cores);

    int iter;
    for (iter = 0; iter < total_iter; iter++) {

        PLASMA_Complex32_t *A1   = (PLASMA_Complex32_t *)malloc(LDA*N*sizeof(PLASMA_Complex32_t));
        PLASMA_Complex32_t *A2   = (PLASMA_Complex32_t *)malloc(LDA*N*sizeof(PLASMA_Complex32_t));
        PLASMA_Complex32_t *B1   = (PLASMA_Complex32_t *)malloc(LDB*NRHS*sizeof(PLASMA_Complex32_t));
        PLASMA_Complex32_t *B2   = (PLASMA_Complex32_t *)malloc(LDB*NRHS*sizeof(PLASMA_Complex32_t));

        /* Check if unable to allocate memory */
        if ((!A1)||(!A2)||(!B1)||(!B2)){
            printf("Out of Memory \n ");
        }

        //printf("-- PLASMA is initialized to run on %d cores. \n",cores);

        /*-------------------------------------------------------------
         *  TESTING CPOSV
         */

        /* Initialize A1 and A2 for Symmetric Positif Matrix (Hessenberg in the complex case) */
        PLASMA_cplghe( (float)N, N, A1, LDA, 51 );
        PLASMA_clacpy( PlasmaUpperLower, N, N, A1, LDA, A2, LDA );

        /* Initialize B1 and B2 */
        PLASMA_cplrnt( N, NRHS, B1, LDB, 371 );
        PLASMA_clacpy( PlasmaUpperLower, N, NRHS, B1, LDB, B2, LDB );

        clock_gettime(CLOCK_MONOTONIC, &start); /* mark start time */ 
        /* PLASMA CPOSV */
        info = PLASMA_cposv(PlasmaUpper, N, NRHS, A2, LDA, B2, LDB);

        clock_gettime(CLOCK_MONOTONIC, &end); /* mark the end time */ 

        /* Difference in seconds */
        diff += (((end.tv_sec - start.tv_sec) + (double)(end.tv_nsec - start.tv_nsec)/BILLION));
        free(A1); free(A2); free(B1); free(B2);
    }
    printf ("Time of decomposition using PLASMA with size %ld : \t %g seconds \n", n, diff);



    PLASMA_Finalize();

    return diff/total_iter;
}
