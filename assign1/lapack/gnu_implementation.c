#include <stdio.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <time.h>

#include "gnu_implementation.h"

#define BILLION 1000000000L

double benchmark_gnu(size_t n, int total_iter) {

    /* Benchmark */
    double diff = 0;
    struct timespec start, end;

    int iter;
    for (iter = 0; iter < total_iter; iter++) {
        /* Create matrix */
        size_t ix, jx;
        gsl_matrix * A = gsl_matrix_alloc(n,n);
        for (ix = 0; ix < n; ix++) {
            gsl_matrix_set(A, ix, ix, 1);
        }




        clock_gettime(CLOCK_MONOTONIC, &start); /* mark start time */ 

        gsl_linalg_cholesky_decomp(A);

        clock_gettime(CLOCK_MONOTONIC, &end); /* mark the end time */ 

        /* Difference in seconds */
        diff += (((end.tv_sec - start.tv_sec) + (double)(end.tv_nsec - start.tv_nsec)/BILLION));
        free(A);
    }
    printf ("Time of decomposition using GNU with size %ld : \t %g seconds \n", n, diff);


    return diff/total_iter;

}
