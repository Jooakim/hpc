#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include <string.h>
#include <png.h>
#include <stdarg.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>

#define BILLION 1000000000L

struct png_info {
    png_structp png_ptr;
    png_structp png_ptr2;
    png_infop info_ptr;
    png_infop info_ptr2;
    int depth;
    int size;
    double ** iterations;
    FILE * fp;
    FILE * fp2;
};

struct thread_data {
    void * thread_id;
    double complex ** roots;
    double ** iterations;
    int size;
    double exponent;
    double eps;
    struct png_info png_data;
};

double complex function(double complex, double);
double complex derivative(double complex, double);
double complex newton_iteration(double complex , double, double);
double complex * newton_iteration_iter(double complex , double, double);
void distribute_rows(struct thread_data *);
void calc_row(struct thread_data *, int);
void generate_png_struct(struct png_info *, int, int);
void write_rows(struct thread_data * thread_data, int, int);

void * thread_helper(void *);
void * painter_thread(void *);


int pixel_size = 3;
static int rows_global = 0;
static int rows_finished = 0;
static int * rows_ready_to_paint;

static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t condition_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t  condition_cond  = PTHREAD_COND_INITIALIZER;

/* For cond_signal and cond_wait in painter thread */
static int condition = 0;


int main(int argc, char *argv[]) {
    int nbr_of_threads;
    double exponent;

    if (argc < 3) {
        printf ("Too few input arguments, must supply both number of threads and exponent \n");
        printf ("Example input: ./newton_pngs -t4 3 \n");
        return 0;
    }

    char * temp;
    temp = argv[1];
    /* Convert char-format digit to int */
    nbr_of_threads = temp[2] - '0';
    if (strlen(temp) > 3) {
        nbr_of_threads *= 10;
        nbr_of_threads += temp[3] - '0';
    } else if (nbr_of_threads < 2) {
        printf ("Too few threads, must have atleast two threads.\n");
        return 0;
    }
    temp = argv[2];
    exponent = temp[0] - '0';

    if (exponent == 0) {
        printf ("Invalid exponent \n");
    }
    if (strlen(temp) > 1) {
        exponent *= 10;
        exponent += temp[1] - '0';
    }
    int n = 1000;


    /* Create matrix */
    double complex **roots = (double complex **)malloc(n * sizeof *roots);
    double  **iterations = (double**)malloc(n * sizeof *roots);
    size_t ix;
    for (ix = 0; ix < n; ix++) {
        roots[ix] = (double complex *)malloc(n * sizeof *roots[ix]);
        iterations[ix] = (double*)malloc(n * sizeof *roots[ix]);
    }

    rows_ready_to_paint = (int*) malloc(n*sizeof(int));


    double eps = 1e-2;

    /* Create information for the png and the roots */
    struct png_info my_data;
    generate_png_struct(&my_data, n, exponent);
    struct thread_data thread_data;
    thread_data.roots = roots;
    thread_data.iterations = iterations;
    thread_data.size = n;
    thread_data.exponent = exponent;
    thread_data.eps = eps;
    thread_data.png_data = my_data;

    /* Create threads */
    pthread_t threads[nbr_of_threads];
    size_t i;
    for (i = 0; i < nbr_of_threads-1; i++) {
        pthread_create(&threads[i], NULL, thread_helper, &thread_data);

    }
    pthread_create(&threads[i], NULL, painter_thread, &thread_data);


    pthread_exit(NULL);
    return 0;
}

void * thread_helper(void * threadarg) {
    struct thread_data *my_data;
    my_data = (struct thread_data *) threadarg;
    distribute_rows(my_data);

    pthread_exit(NULL);
}

void * painter_thread(void * threadarg) {
    struct thread_data *my_data;
    int iter_cnt = 0;

    my_data = (struct thread_data *) threadarg;
    while (rows_finished < my_data->size) {

        /* Introducing a condition to avoid lost-wake-up-signal */
        if (condition == 0) {
            pthread_cond_wait(&condition_cond, &condition_mutex);
        }
        condition = 0;
        iter_cnt++;
        int n_rows = 0;
        while (rows_finished < my_data->size && rows_ready_to_paint[rows_finished]) {
            rows_finished++;
            n_rows++;
        }
        write_rows(my_data, rows_finished-n_rows, n_rows);
    }
    struct png_info * png_data = &my_data->png_data;

    fclose(png_data->fp);
    fclose(png_data->fp2);

    pthread_exit(NULL);
}

double complex function(double complex x, double exponent) {
    return cpow(x,exponent) - 1;
}

double complex derivative(double complex x, double exponent) {
    return exponent * cpow(x, exponent - 1);
}

/**
 * Runs Newton-Raphson iteratively until a root within 
 * threshold epilon is found
 **/
double complex * newton_iteration_iter(double complex x, double exponent, double eps) {
    double complex ratio, x1 = x;
    double complex * values = malloc(2*sizeof(double complex));
    int n_iter = 0;

    do {
        x = x1;
        ratio = function(x, exponent)/derivative(x, exponent);
        x1 = x - ratio;
        n_iter++;
    } while (cabs(x1-x) > eps);
    values[0] = x1;
    values[1] = n_iter;

    return values;
}

/**
 * Load Balencing in form of a dynamic scheduler.
 * As soon as one thread has finished it's current row
 * it gets assigned a new row until all rows has been 
 * assigned.
 **/
void distribute_rows(struct thread_data * my_data) {
    while (rows_global < my_data->size) {
        pthread_mutex_lock(&lock);
        rows_global++;
        pthread_mutex_unlock(&lock);
        calc_row(my_data, rows_global - 1);
    }
}

void calc_row(struct thread_data * my_data, int row) {
    size_t col;
    double im = 2 - (double)4*row/my_data->size;
    for (col = 0; col < my_data->size; col++) {
        double re = (double) 4*col/my_data->size - 2;
        double complex * temp;
        temp = newton_iteration_iter(re + im*I, my_data->exponent, my_data->eps);
        my_data->roots[row][col] = temp[0];//creal(temp[0]);
        my_data->iterations[row][col] = creal(temp[1]);
        free(temp);
    }
    pthread_mutex_lock(&lock);
    rows_ready_to_paint[row] = 1;
    pthread_mutex_unlock(&lock);
    if (row == rows_finished) {
        pthread_mutex_lock(&condition_mutex);
        condition = 1;
        pthread_mutex_unlock(&condition_mutex);
        pthread_cond_signal( &condition_cond );
    }
} 

void write_rows(struct thread_data * thread_data, int y, int n_rows) {
    struct png_info * png_data = &thread_data->png_data;
    size_t ix,jx;
    png_byte *row = 
        png_malloc (png_data->png_ptr, png_data->size * pixel_size);
    png_byte *row_iter = 
        png_malloc (png_data->png_ptr2, png_data->size);
    for (ix = y; ix < y+n_rows; ++ix) {
        int index = 0;
        for (jx = 0; jx < png_data->size; ++jx) {
            /* Red value 10, 50 and 100 are for appropritae scaling*/
            double complex ** temp = thread_data->roots;
            row[index++] = (100*creal(temp[ix][jx]));
            /* Green value */
            row[index++] = (100*cimag(temp[ix][jx]));
            /* Blue value */
            row_iter[jx] = 10*thread_data->iterations[ix][jx];
            row[index++] = 50*(creal(temp[ix][jx])+cimag(temp[ix][jx]));
        }
        png_write_row(png_data->png_ptr, row);
        png_write_row(png_data->png_ptr2, row_iter);
    }
    free(row); free(row_iter);
}

void generate_png_struct(struct png_info * my_data, int size, int exponent) {
    char fn[30];
    char fn2[30];
    snprintf (fn, sizeof fn, "newton_attractors_x%d.png", exponent);
    snprintf (fn2, sizeof fn2, "newton_convergence_x%d.png", exponent);

    FILE *fp = fopen(fn, "wb");
    FILE *fp2 = fopen(fn2, "wb");

    png_structp png_ptr = png_create_write_struct
        (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

    png_infop info_ptr = png_create_info_struct(png_ptr);

    png_structp png_ptr2 = png_create_write_struct
        (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

    png_infop info_ptr2 = png_create_info_struct(png_ptr2);



    /* Set image attributes. */
    int depth = 8;
    png_set_IHDR (png_ptr,
            info_ptr,
            size,
            size,
            depth,
            PNG_COLOR_TYPE_RGB,
            PNG_INTERLACE_NONE,
            PNG_COMPRESSION_TYPE_DEFAULT,
            PNG_FILTER_TYPE_DEFAULT);

    png_set_IHDR (png_ptr2,
            info_ptr2,
            size,
            size,
            depth,
            PNG_COLOR_TYPE_GRAY,
            PNG_INTERLACE_NONE,
            PNG_COMPRESSION_TYPE_DEFAULT,
            PNG_FILTER_TYPE_DEFAULT);

    png_init_io (png_ptr, fp);
    png_init_io (png_ptr2, fp2);
    png_write_info(png_ptr, info_ptr);
    png_write_info(png_ptr2, info_ptr2);


    my_data->fp = fp;
    my_data->fp2 = fp2;
    my_data->png_ptr = png_ptr;
    my_data->png_ptr2 = png_ptr2;
    my_data->info_ptr = info_ptr;
    my_data->info_ptr2 = info_ptr2;
    my_data->size = size;
}
