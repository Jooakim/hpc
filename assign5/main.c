#include <stdio.h>
#include <stdlib.h>

#include <time.h>

#include "gpu_heat_diffusion.h"
#include "cpu_heat_diffusion.h"

#define BILLION 1e9L

void benchmark_gpu(const int, const int,
        const float, const int);
void benchmark_cpu(const int, const int,
        const float, const int);

int main(int argc, char * argv[]) {

    if (argc < 5) {
        printf ("Too few input arguments, must supply both number of threads and exponent \n");
        printf ("Example input: ./heat_diffusion 1000 2000 0.02 20 \n");
        return 0;
    }
    /* Model variables */
    const int height        = atoi(argv[1]);
    const int width         = atoi(argv[2]);
    const float diff_const  = atof(argv[3]);
    const int n_iter        = atoi(argv[4]);

    benchmark_gpu(width, height, diff_const, n_iter);
    benchmark_cpu(width, height, diff_const, n_iter);

    return 0;
}

void benchmark_gpu(const int width, const int height, 
        const float diff_const, const int n_iter) {
    
    double diff = 0;
    struct timespec start, end;

    clock_gettime(CLOCK_MONOTONIC, &start);
    run_gpu_heat_diffusion(width, height, n_iter, diff_const);
    clock_gettime(CLOCK_MONOTONIC, &end);

    diff = ((end.tv_sec - start.tv_sec) + (double)(end.tv_nsec - start.tv_nsec)/BILLION);

    printf ("Time for gpu: %f seconds\n", diff);
}

void benchmark_cpu(const int width, const int height, 
        const float diff_const, const int n_iter) {

    double diff = 0;
    struct timespec start, end;

    clock_gettime(CLOCK_MONOTONIC, &start);
    run_cpu_heat_diffusion(width, height, n_iter, diff_const);
    clock_gettime(CLOCK_MONOTONIC, &end);

    diff = ((end.tv_sec - start.tv_sec) + (double)(end.tv_nsec - start.tv_nsec)/BILLION);

    printf ("Time for cpu: %f seconds \n", diff);

}
