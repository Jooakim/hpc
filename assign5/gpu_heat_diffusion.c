#include <CL/cl.h>
#include <stdio.h>
#include <math.h>

#include "gpu_heat_diffusion.h"

#define MEM_SIZE (128)
#define MAX_SOURCE_SIZE (0x10000)

int run_gpu_heat_diffusion(const int width, const int height,
        const int n_iter, const float diff_const) {

    /* OpenCL variables */
    cl_int error;
    cl_platform_id platform_id;
    cl_command_queue command_queue;
    cl_program program;
    cl_kernel kernel;
    cl_uint nmb_platforms;
    cl_device_id device_id;
    cl_uint nmb_devices;
    cl_context context;
    cl_mem input_buffer_a, output_buffer_a;

    /* Read in code for the kernel */
    FILE *fp;
    char fileName[] = "./diffusion_model";
    char *source_str;
    size_t source_size;
    fp = fopen(fileName, "r");
    source_str = (char*)malloc(MAX_SOURCE_SIZE);
    source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
    fclose(fp);


    /* Create matrix all zeros except middle element */
    float * a = malloc(width*height*sizeof(float));
    for (size_t iy=0; iy<height; ++iy) {
        for (size_t ix=0; ix<width; ++ix) {
            a[iy*width + ix] = 0;
            if (iy == height/2 && ix == width/2) {
                a[iy*width + ix] = 1e6; 
            }
        }
    }


    if (clGetPlatformIDs(1, &platform_id, &nmb_platforms) != CL_SUCCESS) {
        printf( "cannot get platform\n" );
        return 1;
    }

    if (clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_GPU, 1,
                &device_id, &nmb_devices) != CL_SUCCESS) {
        printf( "cannot get device\n" );
        return 1;
    }

    /* Create OpenCL context */
    cl_context_properties properties[] =
    {
        CL_CONTEXT_PLATFORM,
        (cl_context_properties) platform_id,
        0
    };

    /* Create OpenCL context */
    context = clCreateContext(properties, 1, &device_id, NULL, NULL, &error);
    if (error != CL_SUCCESS) {
        printf ("couldn't create context \n");
        return 1;
    }

    /* Create Command Queue */
    command_queue = clCreateCommandQueue(context, device_id, 0, &error);
    if (error != CL_SUCCESS) {
        printf ("couldn't create commandqueue\n");
        return 1;
    }

    /* Create Memory Buffer */
    input_buffer_a  = clCreateBuffer(context, CL_MEM_READ_ONLY,
            sizeof(float) * width*height, NULL, &error);
    output_buffer_a = clCreateBuffer(context, CL_MEM_READ_ONLY,
            sizeof(float) *width*height, NULL, &error);
    if (error != CL_SUCCESS) {
        printf ("couldn't create buffers \n");
        return 1;
    }

    /* Create OpenCL programs */
    program = clCreateProgramWithSource(context,1,
            (const char **) &source_str, (const size_t *)&source_size, &error);
    if (error != CL_SUCCESS) {
        printf ("couldn't create proram \n");
        return 1;
    }

    /* Build OpenCL program */
    error = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    if (error == CL_BUILD_PROGRAM_FAILURE) {
        printf ("couldn't build\n");
        printf ("%d\n", error);
        return 1;
    }

    /* Create OpenCL Kernel */
    kernel = clCreateKernel(program, "heat_diffusion", &error);
    if (error != CL_SUCCESS) {
        printf ("couldn't create kernel \n");
        printf ("%d\n", error);
        return 1;
    }


    /* Run the model for heat diffusion on the gpu */
    for (size_t iter = 0; iter < n_iter; ++iter) {
        clEnqueueWriteBuffer(command_queue, input_buffer_a, CL_TRUE,
                0, width*height*sizeof(float), a, 0, NULL, NULL);

        /* Set OpenCL Kernel Parameters */
        error = clSetKernelArg(kernel, 0, sizeof(float), &diff_const);
        error = clSetKernelArg(kernel, 1, sizeof(int), &width);
        error = clSetKernelArg(kernel, 2, sizeof(int), &height);
        error = clSetKernelArg(kernel, 3, sizeof(cl_mem), &input_buffer_a);
        error = clSetKernelArg(kernel, 4, sizeof(cl_mem), &output_buffer_a);

        const size_t global[] = {width, height};
        /* Execute OpenCL Kernel */
        error = clEnqueueNDRangeKernel(command_queue, kernel, 2, NULL, 
                (const size_t *)&global, NULL, 0, NULL, NULL);

        error = clEnqueueReadBuffer(command_queue, output_buffer_a, CL_TRUE,
                0, width*height*sizeof(float), a, 0, NULL, NULL);
    }

    /* Calculate the absolute average temperature */
    float temp_sum_total = 0;
    for (size_t ix = 0; ix < width*height; ++ix) {
        temp_sum_total += a[ix];
    }
    float average_temp = temp_sum_total/(width*height);
    temp_sum_total = 0;
    for (size_t ix = 0; ix < width*height; ++ix) {
        temp_sum_total += fabs(a[ix]-average_temp);
    }
    float absolute_average_temp = temp_sum_total/(width*height);
    printf ("Absolute average temp: %f\n", absolute_average_temp);

    /* Finalization */
    error = clFinish(command_queue);
    error = clFlush(command_queue);
    error = clReleaseKernel(kernel);
    error = clReleaseProgram(program);
    error = clReleaseMemObject(input_buffer_a);
    error = clReleaseMemObject(output_buffer_a);
    error = clReleaseCommandQueue(command_queue);
    error = clReleaseContext(context);

    free(source_str);
    free(a);

    return 0;
}
