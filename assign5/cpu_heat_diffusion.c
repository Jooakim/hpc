#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "cpu_heat_diffusion.h"

void run_cpu_heat_diffusion(const int width, const int height,
        const int n_iter, const float diff_const) {

    /* Create matrix all zeros except middle element */
    float * a = malloc(width*height*sizeof(float));
    for (size_t iy=0; iy<height; ++iy) {
        for (size_t ix=0; ix<width; ++ix) {
            a[iy*width + ix] = 0;
            if (iy == height/2 && ix == width/2) {
                a[iy*width + ix] = 1e6; 
            }
        }
    }

    /*
     * Update the matrix according to model equation where 
     * squares outside the matrix is considered as a square 
     * with constant temperature 0
     */
    for (size_t iter = 0; iter < n_iter; ++iter) {
        for (size_t iy = 0; iy < height; ++iy) {
            for (size_t ix = 0; ix < width; ++ix) {
                float h1,h2,h3,h4;
                if (ix == 0) {
                    h1 = 0;
                } else {
                    h1 = a[iy*width + ix - 1];
                }

                if (iy == 0) {
                    h2 = 0;
                } else {
                    h2 = a[(iy-1)*width + ix];
                }

                if (ix == width-1) {
                    h3 = 0;
                } else {
                    h3 = a[iy*width + ix + 1];
                }

                if (iy == height-1) {
                    h4 = 0;
                } else {
                    h4 = a[(iy+1)*width + ix];
                }

                a[iy*width + ix] = a[iy*width + ix] + diff_const * ((h1+h2+h3+h4)/4 - a[iy*width + ix]);

            }

        }
    }

    calc_temperatures(width, height, a);
}

/*
 * Calculate the average absolute temperature defined as
 * the average of the absolute difference between each temperature 
 * and the total average temperatue
 */
void calc_temperatures(int width, int height, float * a) {

    float temp_sum_total = 0;
    for (size_t ix = 0; ix < width*height; ++ix) {
        temp_sum_total += a[ix];
    }
    float average_temp = temp_sum_total/(width*height);
    temp_sum_total = 0;
    for (size_t ix = 0; ix < width*height; ++ix) {
        temp_sum_total += fabs(a[ix]-average_temp);
    }
    float absolute_average_temp = temp_sum_total/(width*height);
    printf ("Absolute average temp: %f\n", absolute_average_temp);

}
