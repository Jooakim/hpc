Assignment 5 - OpenCL
====================

## Benchmark

CPU/GPU     Width   Height  Iterations  Time        Absolute Average Temp
-------     -----   -----   ----------  -----       ---------------------
CPU         10      10      10          0.000016s   18739.23
            10      10      2000        0.0011s     1432.66
            10      2000    10          0.0021s     99.89
            10      2000    2000        0.42s       54.34
            2000    2000    10          0.41s       0.50
            2000    2000    2000        81.72s      0.50
GPU         10      10      10          0.40s       18762.48
            10      10      2000        0.88s       1455.08
            10      2000    10          0.39s       99.90
            10      2000    2000        0.72s       54.75
            2000    2000    10          0.49s       0.50
            2000    2000    2000        10.14s      0.50

As we can see in the table above, gpu processing really only shines when the matrix is really large. This is partly because my benchmarking includes everything from setting up the system to calculating the average temperatures. (From the table we can conclude that it takes about 0.4s to set up the kernel and enqueu it). But it is also because the gpu does not handle if-statements very well. When the matrix is small, the edges are a big part of the total number of squares, hence all threads do not take the same path through the if-statement which causes divergence. This is particularly clear in the case of the 10x10 matrix where you maybe would expect the gpu to outperform the cpu since the gpu can compute all the squares parallel, but because of the if-statements the gpu don't do nearly as well as the cpu.

For large matrices the edge is a very tiny portion of the whole matrix. Therefor almost all threads take the same path through the if-statement and the gpu can predict where to go next. As a result we have a runtime of the gpu which is one 1/8 of that of the cpu, profit!


